import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { Page1Component } from './components/page1/page1.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { Routes, RouterModule } from '@angular/router';
import { Page2Component } from './components/page2/page2.component';
import { LazyComponent } from './components/lazy/lazy.component';
import { AuthenticationGuard } from './services/authentication.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'page-1', component: Page1Component, canActivate: [AuthenticationGuard] },
  { path: 'page-2', component: Page2Component, canActivate: [AuthenticationGuard] },
  { path: 'lazy', component: LazyComponent, canActivate: [AuthenticationGuard] }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    Page1Component,
    Page2Component,
    NavigationComponent,
    LazyComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
