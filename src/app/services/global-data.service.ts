import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GlobalDataService {
  data = {
    validUser:false,
    userDetails:{
      name:''
    },
    pages:{
      page1:{
        visited:false,
        visitCount:0
      },
      page2:{
        visited:false,
        visitCount:0
      },
      lazy:{
        visited:false,
        visitCount:0
      }
    }
  }
  constructor() { }
}
