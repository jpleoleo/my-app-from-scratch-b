import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { GlobalDataService } from '../../services/global-data.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {

  constructor(private router:Router, private globalDataService:GlobalDataService) { }

  ngOnInit() {
  }

  navigateTo(page:string){
    this.router.navigateByUrl(page);
  }

  logout(){
    this.globalDataService.data.validUser = false;
    this.navigateTo('login');
  }

}
